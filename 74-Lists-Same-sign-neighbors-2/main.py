# Read a list of integers:
a = [int(s) for s in input().split()]

for num in range(1, len(a)):
  if a[num - 1] * a[num] > 0:
    print(a[num - 1], a[num])
    break
else:
  print(0)